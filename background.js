browser.contextMenus.create(
{
    id: "save-link",
    title: "Save Link",
    contexts: ["all"]
});

browser.storage.local.get().then((data) =>
{   // if nothing in local storage, set default settings.
    if (Object.keys(data).length === 0) 
    {
        let fileType = {"fileType": "url"};
        let contents = {
        "url": "[InternetShortcut]\nURL=%url%",
        "desktop": "[Desktop Entry]\nVersion=1.0\nType=Link\nName=%title%\nURL=%url%",
        "webloc": "<plist version=\"1.0\"><dict>\n<key>URL</key><string>%url%</string>\n</dict></plist>"};
        browser.storage.local.set({fileType,contents}).then(null);
    }
});


browser.contextMenus.onClicked.addListener((info, tab) => 
{
    if (info.menuItemId === "save-link")
    {
        browser.storage.local.get().then((settings) =>
        {
            let extension = settings["fileType"]["fileType"];
            let fileContents = settings["contents"][extension].replace("%url%",tab.url).replace("%title%",tab.title);
            let tabTitleSafe = tab.title.replace(/[^a-z0-9]/gi, '-');
            let fileName = `${tabTitleSafe}.${extension}`;
            let file = new File([fileContents],fileName);
            let objectURL = URL.createObjectURL(file);
            browser.downloads.download({url : objectURL,filename: fileName});
        });
    }
});